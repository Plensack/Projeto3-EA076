#include <Wire.h>
  byte LSB_num=0;
  byte MSB_num =0;
  byte numaux = 0;
  int num = 210;
  int flag = 0;
  int currentAdress = 0;
  
void setup(){
  Wire.begin();
  Serial.begin(9600);
   /*
    Essa memória possui 8 páginas endereçadas pelos bits P0P1P2  e cada página possui 128 palavras, enderaçadas com 8bits.
    Como cada dispotivo no barramento I2C possui um endereço de 7bits, eu entendi que cada página da memória é interpretada 
como um dispositivo diferente com endereço 1010P0P1P2.
    Assim, na função wire.beginTransmission() tem que passar o endereço da página que se quer ler/escrever
      Obs: O oitavo bit (leitura/escrita) é desconsiderado na hora de passar o endereço pois a própria função seta esse bit.
      Ex:   Escrever na página 7:  1010 111 0 ->  1010111 = 0x57
    Em seguida escreve o endereço da posição da memória que vai ser escrita, os n dados necessários e termina a transmissão
  */
  MSB_num = num/256;
  LSB_num = num%256;
  Wire.beginTransmission(0x57);   // O argumento é o endereço de 7bits
  Wire.write(currentAdress);//endereço inicial   
  Wire.write(LSB_num);  
  Wire.endTransmission();
  
  delay(100);
  
  //currentAdress++;
  Wire.beginTransmission(0x57);   // O argumento é o endereço de 7bits
  Wire.write(currentAdress+1);//endereço inicial   
  Wire.write(MSB_num);  
  Wire.endTransmission();
  //currentAdress = 20;
  Serial.println("------------------------------------------");
 // Serial.println(MSB_num*256 + LSB_num);
  num = 0;
  numaux= 0;
}

void loop() {

  delay(1000);
  
  /*
    Não entendi muito bem como ler o dado, tipo, a necessidade de "escrever" algo antes da leitura.
  */
  Wire.beginTransmission(0x57);
  Wire.write(currentAdress); 
  Wire.endTransmission();
  
  // read 1 byte, from address 0
  Wire.requestFrom(0x57, 1);
  while(Wire.available()) {
    numaux = Wire.read();
    //flag = 1;
  }
  
  Wire.beginTransmission(0x57);
  Wire.write(currentAdress+1); 
  Wire.endTransmission();
  
  Wire.requestFrom(0x57, 1);
  while(Wire.available()) {
    num = Wire.read();
    flag = 1;
  }
  

  if(flag){
  Serial.print("OI: ");
  flag = numaux+num*256;
  Serial.println(flag);
  flag = 0;
  }

  }