/*  Projeto 03 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer
#include <Adafruit_GFX.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110
#include <Adafruit_PCD8544.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110
#include <Wire.h> //biblioteca para implementação do barramento I2C e suas funções

//defines
#define base 1000 //base de 1ms
#define pinoInterruptExterna 3 //pino digital a ser definido para receber interrupção externa
#define DC 7 // 
#define CSCE 10 //
#define RST 6 // 
#define L0 A0 // linha 0 do teclado
#define L1 A1 // linha 1 do teclado
#define L2 A2 // linha 2 do teclado
#define L3 9 //  linha 3 do teclado
#define C0 3 //  coluna 0 do teclado
#define C1 4 //  coluna 1 do teclado
#define C2 5 //  coluna 2 do teclado
#define PAGE0 0b1010000 // endereço da página 0 no barramento I2C
#define PAGE1 0b1010001 // endereço da página 1 no barramento I2C
#define PAGE2 0b1010010 // endereço da página 2 no barramento I2C
#define PAGE3 0b1010011 // endereço da página 3 no barramento I2C
#define PAGE4 0b1010100 // endereço da página 4 no barramento I2C
#define PAGE5 0b1010101 // endereço da página 5 no barramento I2C
#define PAGE6 0b1010110 // endereço da página 6 no barramento I2C
#define PAGE7 0b1010111 // endereço da página 7 no barramento I2C
#define ATIVA HIGH //o que significa estar ativa para uma coluna
#define INATIVA LOW // o que significa estar inativa para uma coluna

//funções
void iniciaDisplay(); // faz toda a sequência de definições de pinos como entradas e saidas
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void iniciaLinhas(); // inicia os pinos que serão usados como linhas
void timerInterruptOn(); //ativa a interrução do timer 1
void timerInterruptOff(); // desativa a interrupçaõ do timer 1
void atualizaDisplay(int currentCommand); // função que atualiza o display e é responsável pela FSM implementada
void readLM35(); // leitura do sensor de temperatura LM35
void aplicaVarredura(int row); // aplica a configuração de linhas/ativas e inativas de acordo com o parâmetro dado
void keyboardISR(); // ISR que trata a interrupção do hardware do teclado
void commandEvaluation(int row,int column); //determina o número do comando dada a linha e coluna identificadas
void iniciaLM35(); // Ativa a configuração de referência interna para aumentar a precisão do LM35
int readTemperaturaArmazenada(byte address); //lê a temperatura armazenada na posição 'address' da memória eeprom
void writeTemperatura(byte address, int data); //escreve a temperatura 'data' na posição 'address' da memória eeprom
int readMemCounter(); // lê o contador de dados contido na memória
void writeMemCounter(int memCount); //atualiza o contador de dados contido na memória

//variaveis e objetos globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
Adafruit_PCD8544 display = Adafruit_PCD8544(DC, CSCE, RST); // Cria um objeto para tratar do display usando a interface SPI do ATMEGA328p
int command = 0;//representa o comando recebido no teclado
int tempMeasure = 0; //representa a medida do conversor ADC
int timerVarredura = 0; //timer que implementa a varredura
int currentRow = 0; //linha corrente
bool isKeyboardInterrupt = false;//flag que identifica se houve interrupção do teclado
int currentColumn = 0;//coluna corrente
int commandRow = 0; //variavel que armazena a linha onde o comando foi identificado
bool isError = false; //indica caso haja erro na leitura do teclado, o que invalida o comando identificado
int COData = 0; //Armazena o estado da coluna 0 na interrupção
int C1Data = 0; //Armazena o estado da coluna 1 na interrupção
int C2Data = 0; //Armazena o estado da coluna 2 na interrupção
bool interrompeVarredura = false; //flag que interrompe a varredura
bool isNewCommand = false; //flag que identifica se há novo comando
int lastCommand = 0; //armazena o último comando válido recebido
int IHMState = 0; //armazena o estado da FSM implementada na IHM
bool isFirstRide = true; //flag que indica que é a primeira vez que o programa está rodando
int validCommand = 0;//armazena o último comando válido
bool isValid = false; //determina se o comando é válido ou não
int temperatura = 0; // armazena o valor inteiro da temperarura, que é armazenado na memória
float temperaturaFloat; //variavél que armazena o valor da temperatura em float aprensetado ao usuário
int timer2Sec = 0; // variável que implementa o timer de 2 segundos para aquisição de dados quando há necessidade
bool isCollecting = false; //flag que determina se estão sendo coletados ou não dados
int dataAmount; //quantidade de dados armazenados
bool allowPrinting = false; //variavel que permite a impressão dos dados no monitor serial

void setup() {
 iniciaDisplay(); //inicia o display
 iniciaLinhas(); // inicia as linhas do teclado
 iniciaLM35(); //inicia o LM35 com Vref = 1,1V
 timerInterruptOn(); //ativa a interrupção do timer
 attachInterrupt(digitalPinToInterrupt(2),keyboardISR,RISING); //ativa a ISR do teclado no pino 2 (Veja o Hardware implementado no vídeo)
 Serial.begin(9600);//inicia a comunicação serial
 Wire.begin(); // inicia o barramento I2C
}

void loop() {
   
    if(!isNewCommand && !isKeyboardInterrupt && timerVarredura>=33){//permite a aplicação da varredura quando não há comandos novos, interrupção do teclado e o timer ainda não excedeu 33ms
    currentRow++; //incrementa a linha corrente
    if(currentRow>=4){//retorna ao início caso ultrapasse o limite
      currentRow = 0;
      }
    aplicaVarredura(currentRow); //aplica a varredura na linha calculada
    timerVarredura = 0;//reinicia o timer
    }
  
  if(isNewCommand && timeCounter>=1000){//atualiza o display sempre que há um novo comando e já se passou 1 segundo
          
      if(isValid && (IHMState == 1 && (lastCommand <= 6 && (validCommand == 10 || validCommand == 12)) || IHMState == 0 && (validCommand <=6) || isFirstRide)){//só atualiza o display caso a condição respeite a FSM implementada na IHM        
        isFirstRide = false;//já trata o caso de primeira passagem
        atualizaDisplay(validCommand);//atualiza o display com o comando válido recebido
        }

      timeCounter = 0;//reinicia o timer
      isNewCommand = false;//indica que o novo comando foi tratado
    }

  if(allowPrinting && readMemCounter() != 0){//autoriza a impressão de dados caso haja algum na memória e o comando 6 tenha sido dado
    allowPrinting = false;//bloqueia que entre de novo nesse condicional sem um novo comando 6
    dataAmount = readMemCounter(); //obtem a quantidade de dados
    int i =0; //incia um contador
    while(i < dataAmount){//imprime todos os dados válidos da memória
     Serial.println((float)readTemperaturaArmazenada(2*i)/10);
     i++;
    }
    }

  
  if(timer2Sec>=2000 && isCollecting){//timer de 2segundos responsável pela atualização da temperatura medida caso deva coletar(autorizado pelo comando 4 e cancelado pelo 5), realiza a escrita desse dado na memória
    readLM35();//leitura da temperatura 
    dataAmount = readMemCounter();//recebe a quantidade atual de dados
    writeTemperatura(dataAmount*2,temperatura); //determina a posição da memória e a temperatura a ser armazenada
    delay(100); // tempo para que a escrita seja terminada
    writeMemCounter(dataAmount + 1);//atualiza a quantidade de dados válidos
    timer2Sec=0;//reinicia o timer de 2 segundos
  }
  
  if(isKeyboardInterrupt && !isNewCommand){//trata o caso de que haja interrupção do teclado e não haja comando novo ainda não tratado
    if(COData == ATIVA && C1Data == INATIVA && C2Data == INATIVA){ //identifica coluna 0
      currentColumn = 1;
      isError = false;
      } else if(COData == INATIVA && C1Data == ATIVA && C2Data == INATIVA){//identifica coluna 1
      currentColumn = 2;
      isError = false;
      }else if(COData == INATIVA && C1Data == INATIVA && C2Data == ATIVA){//identifica coluna 2
      currentColumn = 3;
      isError = false;
      } else{//trata o caso de erro do XOR
        isError = true;//devido ao hardware, temos que a única forma de acontecer erro é se as 3 colunas forem pressionados
      } 
      
     if(!isError){//caso não seja o caso de exceção do XOR, tratamos a coluna dada
      commandEvaluation(commandRow,currentColumn);//aplica a avaliação do comando para determinar qual tecla foi pressionada (1 à 12)
      if((command <= 9 && command >= 7) || (command == 11)){//ignora os comandos 7,8,9 e 11, pois não representam nada para o sistema
        isValid = false;
      } else {
        if((IHMState==0 && command<=6)){//só recebe menor que 6 no estado 0
          validCommand = command;
          isValid = true;//indica que é válido
          }
        if((IHMState==1 && command==10)||(IHMState==1 && command==12)){//só recebe 10 ou 12 no estado 1
          lastCommand = validCommand;//salva o comando que colocou o sistema no estado 2
          validCommand = command; //salva a confirmação ou o cancelamento
          isValid = true; //indica que é válido 
        }
        }
      }
     isKeyboardInterrupt = false;//indica que a interrupção do teclado foi tratada
     isNewCommand = true;//indica que há um novo comando para atualizar no display
  }
}

int readTemperaturaArmazenada(byte address){
  int temp_MSB; //armazena os mais significativos do valor da temperatura
  int temp_LSB; // armazena os menos significativos do valor da temperatura
  byte page = address/2048; //determina a qual página de 2K
  byte in_page_address = address%2048; // determina o endereço dentro da página de 2K
  byte mem_page; //armazena o endereço usado no I2C definido pelo switch abaixo
  
  switch(page){
    case 0:
      mem_page = PAGE0;
      break;
    
    case 1:
      mem_page = PAGE1;
      break;
    
    case 2:
      mem_page = PAGE2;
      break;
    
    case 3:
      mem_page = PAGE3;
      break;
    
    case 4:
      mem_page = PAGE4;
      break;
    
    case 5:
      mem_page = PAGE5;
      break;
    
    case 6:
      mem_page = PAGE6;
      break;

    case 7:
      mem_page = PAGE7;
      break;
    }
  
  //requisita o dado da memória da pagina na posição LSB
  Wire.beginTransmission(mem_page);//seleciona a página da memória à ser escrita
  Wire.write(in_page_address);//posicao da parte 'LSB' da medida de temperatura
  Wire.endTransmission();
  
  // obtem o byte menos significativo
  Wire.requestFrom(mem_page,1);
  while(Wire.available()) {
    temp_LSB = Wire.read();
  }
  
  //requisita o dado da memória da pagina na posição MSB
  Wire.beginTransmission(mem_page);
  Wire.write(in_page_address + 1);//posicao da parte 'MSB' da medida de temperatura
  Wire.endTransmission();
  
  // obtem o byte mais significativo
  Wire.requestFrom(mem_page,1);
  while(Wire.available()) {
    temp_MSB = Wire.read();
  }

  return temp_LSB + 256*temp_MSB;//retorna o valor armazenado de temperatura
  }

void writeTemperatura(byte address, int data){
  int data_MSB = data/256;//separa os MSBs do dado
  int data_LSB = data%256;//separa os LSBs do dado
  byte page = (address/2048)%8; // identifica a página (corrigindo caso a quantidade de dados ultrapasse as 7 páginas)
  byte in_page_address = address%2048;//identifica o endereço dentro da página 
  byte mem_page; // armazena o endereço da página de memória no barramento I2C

  if(page == 7 && in_page_address == 2046){
    return; //ignora a escrita caso ele vá sobre escrever o valor do contador de dados
    }
  
  switch(page){//atribui o endereço correspondente à mem_page
    case 0:
      mem_page = PAGE0;
      break;
    
    case 1:
      mem_page = PAGE1;
      break;
    
    case 2:
      mem_page = PAGE2;
      break;
    
    case 3:
      mem_page = PAGE3;
      break;
    
    case 4:
      mem_page = PAGE4;
      break;
    
    case 5:
      mem_page = PAGE5;
      break;
    
    case 6:
      mem_page = PAGE6;
      break;

    case 7:
      mem_page = PAGE7;
      break;
    }
        
  Wire.beginTransmission(mem_page);   // O argumento é o endereço de 7bits
  Wire.write(in_page_address); //o edereço dos dados LSB
  Wire.write(data_LSB);//escreve o dado
  Wire.endTransmission(); // finaliza a transmissão
  
  delay(100);//delay pra permitir que a escrita seja concluida
  
  Wire.beginTransmission(mem_page);   // O argumento é o endereço de 7bits
  Wire.write(in_page_address + 1);//escreve na 2046   
  Wire.write(data_MSB); //escreve o dado
  Wire.endTransmission(); //finaliza transmissão
  }

int readMemCounter(){
  int posicao_MSB;//quantidade MSB
  int posicao_LSB; //quantidade LSB
  
  Wire.beginTransmission(PAGE7);//está na página 7
  Wire.write(0x7FE);//posicao 2046
  Wire.endTransmission(); // finaliza transmissão
  
  // obtem o byte menos significativo
  Wire.requestFrom(PAGE7,1); // requisita o dado da página solicitada
  while(Wire.available()) {//aguarda o recebimento
    posicao_MSB = Wire.read();
  }

  Wire.beginTransmission(PAGE7);
  Wire.write(0x7FF);//posicao 2047
  Wire.endTransmission();
  
  // obtem o byte mais significativo
  Wire.requestFrom(PAGE7,1);//requisita o dado da página 7
  while(Wire.available()) {//aguarda o recebimento
    posicao_LSB = Wire.read();
  }

  return posicao_LSB + 256*posicao_MSB;//retorna o lido, que indica a quantidade de dados válidos
}

void writeMemCounter(int memCount){
  int mem_MSB = memCount/256;
  int mem_LSB = memCount%256;

  Wire.beginTransmission(0x57);   // O argumento é o endereço de 7bits
  Wire.write(0x7FF);//escreve na 2047   
  Wire.write(mem_LSB);  
  Wire.endTransmission();
  
  delay(100);//delay pra permitir que a escrita seja concluida
  
  Wire.beginTransmission(0x57);   // O argumento é o endereço de 7bits
  Wire.write(0x7FE);//escreve na 2046   
  Wire.write(mem_MSB);  
  Wire.endTransmission();
  
  }
 
void iniciaLM35(){
 //admitindo que o sensor produz a seguinte formula: Vout = Temperatura*10mV/C: 
 analogReference(INTERNAL);//definimos a tensão de referência do arduino como sendo de 1,1V, o que aumenta a precisão da conversão do ADC para 1,07 mV
 //sendo assim, podemos medir temperaturas de até 110 C, como o projeto é um medidor de temperatura ambiente, muito raramente as temperaturas passarão atingirão esse valor
 //apresentado uma boa precisão para os valores de interesse. 
 }

void commandEvaluation(int row,int column){
  command = (row*3) + column;
  }

void iniciaLinhas(){//inicia todas as linhas como saídas digitais
 pinMode(L3,OUTPUT);
 pinMode(L2,OUTPUT);
 pinMode(L1,OUTPUT);
 pinMode(L0,OUTPUT);
  }
  
void keyboardISR(){
  if(!isKeyboardInterrupt){//verifica se última interrupção já foi tratada
    isKeyboardInterrupt = true;//indica que há interrupção
    COData = digitalRead(C0);//salva os valores das três colunas
    C1Data = digitalRead(C1);
    C2Data = digitalRead(C2);
    commandRow = currentRow;//salva o valor linha corrente
  }
}


void aplicaVarredura(int row){//aplica 0V na linha selecionada, de modo a apresentar circuito fechado quando o botão for pressionado
  if (row == 0){
    digitalWrite(L0,LOW);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,HIGH);
    }

   if (row == 1){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,LOW);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,HIGH);    
    }
    
    if (row == 2){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,LOW);
    digitalWrite(L3,HIGH);
    }

    if (row == 3){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,LOW);
    }
}


void readLM35(){//leitura do LM35 e armazengem dos 3 valores de interesse
  tempMeasure = analogRead(3);//armazena o valor da conveersão ADC
  temperaturaFloat = ((1000*1.1*0.1*float(tempMeasure)/1023));//reliza a conversão para inteiro de 10*temp(ºC)
  temperatura = ((int)(1000*1.1*float(tempMeasure)/1023)); //realiza a conversão pra float em xx,xºC
  }

void atualizaDisplay(int currentCommand){//atualiza o display e implementa a FSM usada na IHM
  display.clearDisplay();//reseta os valores do display
  
  if (currentCommand == 1 && IHMState == 0){//indica que o comando 1 foi recebido no estado 0
     IHMState = 1;//vai para o estado 1
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Reset");//linha 3
     display.println("Apply Cmd ?");// linha 4
     display.println("(Y/N)->(#/*)");//linha 5
     }
   
   if (currentCommand == 2 && IHMState == 0){//indica que o comando 2 foi recebido no estado 0
     IHMState = 1;//vai para o estado 1
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Measure");//linha 3
     display.println("Apply Cmd ?");// linha 4
     display.println("(Y/N)->(#/*)");//linha 5
      }

   if (currentCommand == 3 && IHMState == 0){//indica que o comando 3 foi recebido no estado 0
     IHMState = 1;//vai para o estado 1
     di0splay.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Status");//linha 3
     display.println("Apply Cmd ?");//linha 4
     display.println("(Y/N)->(#/*)");
      }

   if (currentCommand == 4 && IHMState == 0){//indica que o comando 4 foi recebido no estado 0
     IHMState = 1;//vai para o estado 1
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Init. Collect");//linha 3
     display.println("Apply Cmd ?");//linha 4
     display.println("(Y/N)->(#/*)");//linha 5
      }


   if (currentCommand == 5 && IHMState == 0){//indica que o comando 5 foi recebido no estado 0
     IHMState = 1;//vai para o estado 1
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Finish Collect");//linha 3
     display.println(" ");//linha 4
     display.println("Apply Cmd ?");// linha 5
     display.println("(Y/N)->(#/*)");
      }


   if (currentCommand == 6 && IHMState == 0){//indica que o comando 6 foi recebido no estado 0
     IHMState = 1;//vai para o estado 1
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Data Transfer");//linha 3
     display.println(" ");//linha 4
     display.println("Apply Cmd ?");// linha 5
     display.println("(Y/N)->(#/*)");
      }  

  if (currentCommand == 10 && lastCommand<=6 && IHMState == 1){//no estado 2, recebeu a confirmação
     IHMState = 2;//vai para o estado 2
     }

  if (currentCommand == 12 && lastCommand<=6 && IHMState == 1){//no estado 2, recebeu o cancelamento
     IHMState = 0;//retorna ao estado 0
     display.println("Proj3 - EA076"); // linha 1
     display.println("O comando "+ String(lastCommand));//linha 2
     display.println("foi cancelado"); // linha 3
     }

  if(IHMState == 2 && lastCommand == 1){//indica que o comando 1 foi executado no estado 2
    display.println("Proj3 - EA076"); // linha 1
    display.println("Reset Memory");//linha 2
    writeMemCounter(0);
    IHMState = 0;//retorna ao estado 0
    }

  if(IHMState == 2 && lastCommand == 2){//indica que o comando 2 foi executado no estado 2
    readLM35();
    display.println("Proj3 - EA076"); // linha 1
    display.println("Measuring:");//linha 2
    display.print(temperaturaFloat,1); // linha 3
    display.println(" C");//linha 3
    IHMState = 0;//retorna ao estado 0
    }

  if(IHMState == 2 && lastCommand == 3){//indica que o comando 3 foi executado no estado 2
      display.println("Proj3 - EA076"); // linha 1
      display.println("Status");//linha 2
      display.println("Quantidade de Dados:");
      display.println(readMemCounter());
      IHMState = 0;//retorna ao estado 0
      }

  if(IHMState == 2 && lastCommand == 4){//indica que o comando 4 foi executado no estado 2
    display.println("Proj3 - EA076"); // linha 1
    display.println("Start");//linha 2
    display.println("Collecting");//linha 3
    isCollecting = true;
    IHMState = 0;//retorna ao estado 0
    }

  if(IHMState == 2 && lastCommand == 5){//indica que o comando 5 foi executado no estado 2
    display.println("Proj3 - EA076"); // linha 1
    display.println("Stop");//linha 2
    display.println("Collecting");//linha 3
    isCollecting = false;
    IHMState = 0;//retorna ao estado 0
    }
   
  if(IHMState == 2 && lastCommand == 6){//indica que o comando 6 foi executado no estado 2
    display.println("Proj3 - EA076"); // linha 1
    display.println("Transfering...");//linha 2
    display.println("");
    display.println("Se existir");
    display.println("pelo menos");
    display.println("1 medida");
    allowPrinting = true;
    IHMState = 0;//retorna ao estado 0
    }
  
  display.display(); //apresenta os valores no display
 }

void iniciaDisplay(){//inicialização do display, como no experimento 2
  display.begin();
  display.setContrast(50); //Ajusta o contraste do display
  display.clearDisplay();   //Apaga o buffer e o display
  display.setTextSize(1);  //Seta o tamanho para caber na linha como o desejado
  display.setTextColor(BLACK); //Seta letra preta
  display.setCursor(0,0);  //Seta a posição do cursor
  display.println("Proj3 - EA076"); // linha 1
  display.println("Comando Atual:"); // linha 2
  display.println(" ");//linha 3
  display.println("MENSAGEM");//linha 4
  display.println("DADOS");// linha 5
  display.display();// apresenta os dados na tela
 }

void increaseTime(){//interrupção temporizada a cada 0,001s
  timeCounter++;
  timerVarredura++;
  timer2Sec++;
}

void timerInterruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void timerInterruptOff(){//desliga as interrupcoes do timerOne
  Timer1.detachInterrupt(); 
  }
