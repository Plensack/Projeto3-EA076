/*  Projeto 03 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer
#include <Adafruit_GFX.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110
#include <Adafruit_PCD8544.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110
#include <Wire.h> //biblioteca para implementação do barramento I2C e suas funções

//defines
#define base 1000 //base de 1ms
#define pinoInterruptExterna 3 //pino digital a ser definido para receber interrupção externa
#define DC 7 // 
#define CSCE 10 //
#define RST 6 // 
#define L0 A0
#define L1 A1
#define L2 A2
#define L3 9
#define C0 3
#define C1 4
#define C2 5
#define PAGE7 0x57

//funções
void iniciaDisplay(); // faz toda a sequência de definições de pinos como entradas e saidas
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void iniciaLinhas();
void timerInterruptOn(); //ativa a interrução do timer 1
void timerInterruptOff(); // desativa a interrupçaõ do timer 1
void atualizaDisplay(int currentCommand,int sensorMeasure, int dataAmount,int dataSpace);
void readLM35();
void aplicaVarredura(int row);
void keyboardISR();
void commandEvaluation(int row,int column);
void iniciaLM35();
int readTemperaturaArmazenada(byte mem_page,byte in_page_address);
void writeTemperatura(byte mem_page,byte in_page_address, int data);
int readMemCounter();
void writeMemCounter(int memCount);

//variaveis e objetos globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
Adafruit_PCD8544 display = Adafruit_PCD8544(DC, CSCE, RST); // Cria um objeto para tratar do display usando a interface SPI do ATMEGA328p
int command = 1;
int tempMeasure = 0;
int timerVarredura = 0;
int currentRow = 0;
bool isKeyboardInterrupt = false;
int currentColumn = 0;
int commandRow = 0;
bool isError = false;
int COData = 0;
int C1Data = 0;
int C2Data = 0;
int ATIVA = HIGH;
int INATIVA = LOW;
bool interrompeVarredura = false;
bool isNewCommand = false;
int lastCommand = 10;
int IHMState = 0;
bool isFirstRide = true;
int validCommand = 0;
bool isValid = false;
int temperatura = 0;
float temperaturaFloat;
int timer2Sec = 0;
bool isCollecting = false;
int dataAmount;
bool allowPrinting = false;

void setup() {
 iniciaDisplay();
 iniciaLinhas();
 iniciaLM35();
 timerInterruptOn();
 attachInterrupt(digitalPinToInterrupt(2),keyboardISR,RISING);
 Serial.begin(9600);//inicia a comunicação serial
 Wire.begin(); // inicia o barramento I2C
}

void loop() {
  
    if(!isNewCommand && !isKeyboardInterrupt && timerVarredura>=33){
    currentRow++;
    if(currentRow>=4){
      currentRow = 0;
      }
    aplicaVarredura(currentRow); 
    timerVarredura = 0;
    }
  
  if(isNewCommand && timeCounter>=1000){
          
      if(isValid && (IHMState == 1 && (lastCommand <= 6 && (validCommand == 10 || validCommand == 12)) || IHMState == 0 && (validCommand <=6) || isFirstRide)){
        isFirstRide = false;
        atualizaDisplay(validCommand);
        }

      timeCounter = 0;
      isNewCommand = false;
    }

  if(allowPrinting && readMemCounter() != 0){
    allowPrinting = false;
    dataAmount = readMemCounter();
    int i =0;
    while(i < dataAmount){
     Serial.println((float)readTemperaturaArmazenada(PAGE7,2*i)/10);
     i++;
    }
    }

  
  if(timer2Sec>=2000){
    readLM35();
    if(isCollecting){
      dataAmount = readMemCounter();
      if(dataAmount == 1023){
        dataAmount = 0;
        }
      writeTemperatura(PAGE7,dataAmount*2,temperatura);
      delay(100);
      writeMemCounter(dataAmount + 1);
      }
    timer2Sec=0;
  }
  
  if(isKeyboardInterrupt && !isNewCommand){
    if(COData == ATIVA && C1Data == INATIVA && C2Data == INATIVA){
      currentColumn = 1;
      isError = false;
      } else if(COData == INATIVA && C1Data == ATIVA && C2Data == INATIVA){
      currentColumn = 2;
      isError = false;
      }else if(COData == INATIVA && C1Data == INATIVA && C2Data == ATIVA){
      currentColumn = 3;
      isError = false;
      } else{
        isError = true;//devido ao hardware, temos que a única forma de acontecer erro é se as 3 colunas forem pressionados
      } 
      
     if(!isError){
      commandEvaluation(commandRow,currentColumn);
      if((command <= 9 && command >= 7) || (command == 11)){
        isValid = false;
      } else {
        if((IHMState==0 && command<=6)){
          validCommand = command;
          isValid = true;
          }
        if((IHMState==1 && command==10)||(IHMState==1 && command==12)){
          lastCommand = validCommand;
          validCommand = command;
          isValid = true;
        }
        }
      }
     isKeyboardInterrupt = false;
     isNewCommand = true;
  }
}

int readTemperaturaArmazenada(byte mem_page,byte in_page_address){
  int temp_MSB;
  int temp_LSB;
  
  Wire.beginTransmission(mem_page);//seleciona a página da memória à ser escrita
  Wire.write(in_page_address);//posicao da parte 'LSB' da medida de temperatura
  Wire.endTransmission();
  
  // obtem o byte mais significativo
  Wire.requestFrom(PAGE7,1);
  while(Wire.available()) {
    temp_LSB = Wire.read();
  }

  Wire.beginTransmission(PAGE7);
  Wire.write(in_page_address + 1);//posicao da parte 'MSB' da medida de temperatura
  Wire.endTransmission();
  
  // obtem o byte mais significativo
  Wire.requestFrom(PAGE7,1);
  while(Wire.available()) {
    temp_MSB = Wire.read();
  }

  return temp_LSB + 256*temp_MSB;
  }

void writeTemperatura(byte mem_page,byte in_page_address, int data){
  int data_MSB = data/256;
  int data_LSB = data%256;

  Wire.beginTransmission(mem_page);   // O argumento é o endereço de 7bits
  Wire.write(in_page_address);//escreve na 2047   
  Wire.write(data_LSB);  
  Wire.endTransmission();
  
  delay(100);//delay pra permitir que a escrita seja concluida
  
  Wire.beginTransmission(mem_page);   // O argumento é o endereço de 7bits
  Wire.write(in_page_address + 1);//escreve na 2046   
  Wire.write(data_MSB);  
  Wire.endTransmission();
  }

int readMemCounter(){
  int posicao_MSB;
  int posicao_LSB;
  
  Wire.beginTransmission(PAGE7);
  Wire.write(0x7FE);//posicao 2046
  Wire.endTransmission();
  
  // obtem o byte mais significativo
  Wire.requestFrom(PAGE7,1);
  while(Wire.available()) {
    posicao_MSB = Wire.read();
  }

  Wire.beginTransmission(PAGE7);
  Wire.write(0x7FF);//posicao 2047
  Wire.endTransmission();
  
  // obtem o byte mais significativo
  Wire.requestFrom(PAGE7,1);
  while(Wire.available()) {
    posicao_LSB = Wire.read();
  }

  return posicao_LSB + 256*posicao_MSB;
}

void writeMemCounter(int memCount){
  int mem_MSB = memCount/256;
  int mem_LSB = memCount%256;

  Wire.beginTransmission(0x57);   // O argumento é o endereço de 7bits
  Wire.write(0x7FF);//escreve na 2047   
  Wire.write(mem_LSB);  
  Wire.endTransmission();
  
  delay(100);//delay pra permitir que a escrita seja concluida
  
  Wire.beginTransmission(0x57);   // O argumento é o endereço de 7bits
  Wire.write(0x7FE);//escreve na 2046   
  Wire.write(mem_MSB);  
  Wire.endTransmission();
  
  }
 
void iniciaLM35(){
 //admitindo que o sensor produz a seguinte formula: Vout = Temperatura*10mV/C: 
 analogReference(INTERNAL);//definimos a tensão de referência do arduino como sendo de 1,1V, o que aumenta a precisão da conversão do ADC para 1,07 mV
 //sendo assim, podemos medir temperaturas de até 110 C, como o projeto é um medidor de temperatura ambiente, muito raramente as temperaturas passarão atingirão esse valor
 //apresentado uma boa precisão para os valores de interesse. 
 }

void commandEvaluation(int row,int column){
  command = (row*3) + column;
  }

void iniciaLinhas(){
 pinMode(L3,OUTPUT);
 pinMode(L2,OUTPUT);
 pinMode(L1,OUTPUT);
 pinMode(L0,OUTPUT);
  }
  
void keyboardISR(){
  if(!isKeyboardInterrupt){
    isKeyboardInterrupt = true;
    COData = digitalRead(C0);
    C1Data = digitalRead(C1);
    C2Data = digitalRead(C2);
    commandRow = currentRow;
  }
}


void aplicaVarredura(int row){
  if (row == 0){
    digitalWrite(L0,LOW);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,HIGH);
    }

   if (row == 1){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,LOW);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,HIGH);    
    }
    
    if (row == 2){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,LOW);
    digitalWrite(L3,HIGH);
    }

    if (row == 3){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,LOW);
    }
}


void readLM35(){
  tempMeasure = analogRead(3);
  temperaturaFloat = ((1000*1.1*0.1*float(tempMeasure)/1023));
  temperatura = ((int)(1000*1.1*float(tempMeasure)/1023));
  }

void atualizaDisplay(int currentCommand){
  display.clearDisplay();//reseta os valores do display
  if (currentCommand == 1 && IHMState == 0){
     IHMState = 1;
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Reset");//linha 3
     display.println("Apply Cmd ?");// linha 4
     display.println("(Y/N)->(#/*)");//linha 5
     }
   
   if (currentCommand == 2 && IHMState == 0){
     IHMState = 1;
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Measure");//linha 3
     display.println("Apply Cmd ?");// linha 4
     display.println("(Y/N)->(#/*)");//linha 5
      }

   if (currentCommand == 3 && IHMState == 0){
     IHMState = 1;
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Status");//linha 3
     display.println("Apply Cmd ?");//linha 4
     display.println("(Y/N)->(#/*)");
      }

   if (currentCommand == 4 && IHMState == 0){
     IHMState = 1;
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Init. Collect");//linha 3
     display.println("Apply Cmd ?");//linha 4
     display.println("(Y/N)->(#/*)");//linha 5
      }


   if (currentCommand == 5 && IHMState == 0){
     IHMState = 1;
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Finish Collect");//linha 3
     display.println(" ");//linha 4
     display.println("Apply Cmd ?");// linha 5
     display.println("(Y/N)->(#/*)");
      }


   if (currentCommand == 6 && IHMState == 0){
     IHMState = 1;
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Data Transfer");//linha 3
     display.println(" ");//linha 4
     display.println("Apply Cmd ?");// linha 5
     display.println("(Y/N)->(#/*)");
      }  

  if (currentCommand == 10 && lastCommand<=6 && IHMState == 1){
     IHMState = 2;
     }

  if (currentCommand == 12 && lastCommand<=6 && IHMState == 1){
     IHMState = 0;
     display.println("Proj3 - EA076"); // linha 1
     display.println("O comando "+ String(lastCommand));//linha 2
     display.println("foi cancelado"); // linha 3
     }

  if(IHMState == 2 && lastCommand == 1){
    display.println("Proj3 - EA076"); // linha 1
    display.println("Reset Memory");//linha 2
    writeMemCounter(0);
    IHMState = 0;
    }

  if(IHMState == 2 && lastCommand == 2){
    readLM35();
    display.println("Proj3 - EA076"); // linha 1
    display.println("Measuring:");//linha 2
    display.print(temperaturaFloat,1); // linha 3
    display.println(" C");//linha 3
    IHMState = 0;
    }

  if(IHMState == 2 && lastCommand == 3){
      display.println("Proj3 - EA076"); // linha 1
      display.println("Status");//linha 2
      display.println("Quantidade de Dados:");
      display.println(readMemCounter());
      IHMState = 0;
      }

  if(IHMState == 2 && lastCommand == 4){
    display.println("Proj3 - EA076"); // linha 1
    display.println("Start");//linha 2
    display.println("Collecting");//linha 3
    isCollecting = true;
    IHMState = 0;
    }

  if(IHMState == 2 && lastCommand == 5){
    display.println("Proj3 - EA076"); // linha 1
    display.println("Stop");//linha 2
    display.println("Collecting");//linha 3
    isCollecting = false;
    IHMState = 0;
    }
   
  if(IHMState == 2 && lastCommand == 6){
    display.println("Proj3 - EA076"); // linha 1
    display.println("Transfering...");//linha 2
    display.println("");
    display.println("Se existir");
    display.println("pelo menos");
    display.println("1 medida");
    allowPrinting = true;
    IHMState = 0;
    }
  
  display.display(); //apresenta os valores no display
 }

void iniciaDisplay(){//inicialização do display
  display.begin();
  display.setContrast(50); //Ajusta o contraste do display
  display.clearDisplay();   //Apaga o buffer e o display
  display.setTextSize(1);  //Seta o tamanho para caber na linha como o desejado
  display.setTextColor(BLACK); //Seta letra preta
  display.setCursor(0,0);  //Seta a posição do cursor
  display.println("Proj3 - EA076"); // linha 1
  display.println("Comando Atual:"); // linha 2
  display.println(" ");//linha 3
  display.println("MENSAGEM");//linha 4
  display.println("DADOS");// linha 5
  display.display();// apresenta os dados na tela
 }

void increaseTime(){//interrupção temporizada a cada 0,001s
  timeCounter++;
  timerVarredura++;
  timer2Sec++;
}

void timerInterruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void timerInterruptOff(){//desliga as interrupcoes do timerOne
  Timer1.detachInterrupt(); 
  }
