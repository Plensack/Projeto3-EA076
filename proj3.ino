/*  Projeto 03 - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */

//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer
#include <Adafruit_GFX.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110
#include <Adafruit_PCD8544.h> //bibloteca desenvolvida pela Adafruit para o uso do dispaly do Nokia 5110

//defines
#define base 1000 //base de 1ms
#define pinoInterruptExterna 3 //pino digital a ser definido para receber interrupção externa
#define DC 7 // 
#define CSCE 10 //
#define RST 6 // 
#define L0 A0
#define L1 A1
#define L2 A2
#define L3 9
#define C0 3
#define C1 4
#define C2 5

//funções
void iniciaDisplay(); // faz toda a sequência de definições de pinos como entradas e saidas
void increaseTime(); // ISR que é responsável pela sincronização dos eventos no Loop
void timerInterruptOn(); //ativa a interrução do timer 1
void timerInterruptOff(); // desativa a interrupçaõ do timer 1
void atualizaDisplay(int currentCommand,int sensorMeasure, int dataAmount,int dataSpace);
void readLM35();
void aplicaVarredura(int row);
void keyboardISR();
int COData = 0;
int C1Data = 0;
int C2Data = 0;
int ATIVA = HIGH;
int INATIVA = LOW;
void commandEvaluation(int row,int column);

//variaveis e objetos globais
int timeCounter=0; //variavel contadora do tempo, segundo a formula tempo = timeCounter * base
Adafruit_PCD8544 display = Adafruit_PCD8544(DC, CSCE, RST); // Cria um objeto para tratar do display usando a interface SPI do ATMEGA328p
int command = 0;
int tempMeasure = 0;
int timerVarredura = 0;
int currentRow = 0;
bool isKeyboardInterrupt = false;
int currentColumn = 0;
int commandRow = 0;
bool isError = false;

void setup() {
 iniciaDisplay();
 iniciaLinhas();
 timerInterruptOn();
 attachInterrupt(digitalPinToInterrupt(2),keyboardISR,RISING);
 Serial.begin(9600);//inicia a comunicação serial
}

void loop() {
    if(!isKeyboardInterrupt && timerVarredura>=50){
    currentRow++;
    if(currentRow>=4){
      currentRow = 0;
      }
    aplicaVarredura(currentRow); 
    timerVarredura = 0;
    }
  
  if(!isKeyboardInterrupt && timeCounter>=2000){
      atualizaDisplay(command,0,0,0);
      timeCounter = 0;
    }
  
    
  if(isKeyboardInterrupt){
    if(COData == ATIVA && C1Data == INATIVA && C2Data == INATIVA){
      currentColumn = 1;
      isError = false;
      } else if(COData == INATIVA && C1Data == ATIVA && C2Data == INATIVA){
      currentColumn = 2;
      isError = false;
      }else if(COData == INATIVA && C1Data == INATIVA && C2Data == ATIVA){
      currentColumn = 3;
      isError = false;
      } else{
        isError = true;
      }
    Serial.println(currentColumn);  
     if(!isError){
     commandEvaluation(commandRow,currentColumn);
     }
     isKeyboardInterrupt = false;
  }
  }

void commandEvaluation(int row,int column){
  command = (row*3) + column;
  }

void iniciaLinhas(){
 pinMode(L3,OUTPUT);
 pinMode(L2,OUTPUT);
 pinMode(L1,OUTPUT);
 pinMode(L0,OUTPUT);
  }
void keyboardISR(){
  if(!isKeyboardInterrupt){
    isKeyboardInterrupt = true;
    COData = digitalRead(C0);
    C1Data = digitalRead(C1);
    C2Data = digitalRead(C2);
    commandRow = currentRow;
  }
}


void aplicaVarredura(int row){
  if (row == 0){
    digitalWrite(L0,LOW);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,HIGH);
    }

   if (row == 1){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,LOW);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,HIGH);    
    }
    
    if (row == 2){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,LOW);
    digitalWrite(L3,HIGH);
    }

    if (row == 3){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,LOW);
    }
}

void readLM35(){
  tempMeasure = analogRead(3);
  }

void atualizaDisplay(int currentCommand,int sensorMeasure, int dataAmount,int dataSpace){
  display.clearDisplay();//reseta os valores do display
  if (currentCommand == 1){
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Reset");//linha 3
     display.println(" ");//linha 4
     display.println("Memory Cleared");// linha 5
     }
   
   if (currentCommand == 2){
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Measure");//linha 3
     display.println(sensorMeasure);//linha 4
     display.println("Current Temp");// linha 5
      }

   if (currentCommand == 3){
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Status");//linha 3
     display.println(String(dataAmount)+" "+String(dataSpace));//linha 4
     display.println("Data Info");// linha 5
      }

   if (currentCommand == 4){
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Init. Collect");//linha 3
     display.println(" ");//linha 4
     display.println("Acquiring ...");// linha 5
      }


   if (currentCommand == 5){
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Finish Collect");//linha 3
     display.println(" ");//linha 4
     display.println("Done !");// linha 5
      }


   if (currentCommand == 6){
     display.println("Proj3 - EA076"); // linha 1
     display.println("Current Cmd:"); // linha 2
     display.println("Data Transfer");//linha 3
     display.println(" ");//linha 4
     display.println("Transfering...");// linha 5
      }  
  display.display(); //apresenta os valores no display
 }

void iniciaDisplay(){//inicialização do display
  display.begin();
  display.setContrast(50); //Ajusta o contraste do display
  display.clearDisplay();   //Apaga o buffer e o display
  display.setTextSize(1);  //Seta o tamanho para caber na linha como o desejado
  display.setTextColor(BLACK); //Seta letra preta
  display.setCursor(0,0);  //Seta a posição do cursor
  display.println("Proj3 - EA076"); // linha 1
  display.println("Comando Atual:"); // linha 2
  display.println(" ");//linha 3
  display.println("MENSAGEM");//linha 4
  display.println("DADOS");// linha 5
  display.display();// apresenta os dados na tela
 }

void increaseTime(){//interrupção temporizada a cada 0,001s
  timeCounter++;
  timerVarredura++;
}

void timerInterruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void timerInterruptOff(){//desliga as interrupcoes do timerOne
  Timer1.detachInterrupt(); 
  }
